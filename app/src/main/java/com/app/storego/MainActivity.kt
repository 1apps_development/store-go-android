package com.app.storego

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.app.storego.adapter.MenuAdapter
import com.app.storego.databinding.ActMainBinding
import com.app.storego.databinding.DialogMenuBinding
import com.app.storego.fragment.*
import com.app.storego.util.Common
import com.github.techisfun.android.topsheet.TopSheetDialog
import com.taskly.app.model.MenuItemData

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActMainBinding
    private var itemList = ArrayList<MenuItemData>()
    private lateinit var menuAdapter: MenuAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)
        binding.bottomNavigation.selectedItemId = R.id.ivDashBoard
        menuItemData()
        bottomBarItemNavigation()
        initClickListeners()

        Common.replaceFragment(supportFragmentManager, FragDashBoard(), R.id.fragContainer)


    }

        private fun initClickListeners()
        {
            binding.ivDrawer.setOnClickListener {
                openTopSheetDialog()
            }
        }



    private  fun menuItemData()
    {
        itemList.add(MenuItemData(resources.getString(R.string.dashboard),ResourcesCompat.getDrawable(resources,R.drawable.ic_dashboard,null)!!,true))
        itemList.add(MenuItemData(resources.getString(R.string.orders),ResourcesCompat.getDrawable(resources,R.drawable.ic_orders,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.products),ResourcesCompat.getDrawable(resources,R.drawable.ic_products,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.categories),ResourcesCompat.getDrawable(resources,R.drawable.ic_categories,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.statistics),ResourcesCompat.getDrawable(resources,R.drawable.ic_statistics,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.plans),ResourcesCompat.getDrawable(resources,R.drawable.ic_plans,null)!!,false))
        itemList.add(MenuItemData(resources.getString(R.string.settings),ResourcesCompat.getDrawable(resources,R.drawable.ic_settings,null)!!,false))
    }

    private fun menuSelection(pos: Int) {
        for (j in 0 until itemList.size) {
            itemList[j].isSelect = false

        }
        itemList[pos].isSelect = true
    }


    private fun openTopSheetDialog() {
        val dialog = TopSheetDialog(this)
        val bottomSheetBinding = DialogMenuBinding.inflate(layoutInflater)

        menuAdapter = MenuAdapter(this@MainActivity, itemList) { s: String, i: Int ->
            if (s == "itemClick") {
                val fragment = supportFragmentManager.findFragmentById(R.id.fragContainer)
                dialog.dismiss()

                if (i == 0) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)
                    if (fragment !is FragDashBoard) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragDashBoard(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 1) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.orders)
                    if (fragment !is FragOrders) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragOrders(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 2) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.products)
                    if (fragment !is FragProducts) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragProducts(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 3) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.categories)
                    if (fragment !is FragCategories) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragCategories(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 4) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.statistics)
                    if (fragment !is FragStatistics) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragStatistics(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 5) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.plans)
                    if (fragment !is FragPlans) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragPlans(),
                            R.id.fragContainer
                        )
                    }
                } else if (i == 6) {
                    binding.tvAppBarTitle.text = resources.getString(R.string.settings)
                    if (fragment !is FragSettings) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragSettings(),
                            R.id.fragContainer
                        )
                    }
                }

                for (j in 0 until itemList.size) {
                    itemList[j].isSelect = false

                }
                itemList[i].isSelect = true
                menuAdapter.notifyDataSetChanged()
            }

        }
        bottomSheetBinding.ivClose.setOnClickListener {
            dialog.dismiss()
        }
        bottomSheetBinding.rvMenu.apply {
            layoutManager = GridLayoutManager(this@MainActivity, 4)
            adapter = menuAdapter
            isNestedScrollingEnabled = true
        }
        dialog.setContentView(bottomSheetBinding.root)
        dialog.show()
    }




    private fun bottomBarItemNavigation() {
        binding.bottomNavigation.setOnItemSelectedListener { item ->
            val fragment = supportFragmentManager.findFragmentById(R.id.fragContainer)
            when (item.itemId) {
                R.id.ivOrder -> {
                    binding.tvAppBarTitle.text = resources.getString(R.string.orders)
                    menuSelection(1)
                    if (fragment !is FragOrders) {
                        supportFragmentManager.fragments.clear()
                        Common.replaceFragment(
                            supportFragmentManager,
                            FragOrders(),
                            R.id.fragContainer
                        )
                    }

                    true


                }
                R.id.ivProducts -> {
                    menuSelection(2)

                    binding.tvAppBarTitle.text = resources.getString(R.string.products)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragProducts(),
                        R.id.fragContainer
                    )

                    true
                }
                R.id.ivDashBoard -> {
                    menuSelection(0)

                    binding.tvAppBarTitle.text = resources.getString(R.string.dashboard)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragDashBoard(),
                        R.id.fragContainer
                    )

                    true
                }
                R.id.ivCategories -> {
                    menuSelection(3)

                    binding.tvAppBarTitle.text = resources.getString(R.string.categories)

                    Common.replaceFragment(supportFragmentManager, FragCategories(), R.id.fragContainer)

                    true
                }

                R.id.ivSetting -> {
                    menuSelection(6)

                    binding.tvAppBarTitle.text = resources.getString(R.string.settings)

                    Common.replaceFragment(
                        supportFragmentManager,
                        FragSettings(),
                        R.id.fragContainer
                    )

                    true
                }
                else -> {
                    false
                }
            }
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finishAffinity()
        }else
        {
            supportFragmentManager.popBackStack()
            super.onBackPressed()
        }
    }

    fun setAppBarTitle(title:String)
    {
        binding.tvAppBarTitle.text=title
    }
}