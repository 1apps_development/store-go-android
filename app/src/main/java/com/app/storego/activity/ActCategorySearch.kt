package com.app.storego.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.storego.adapter.SearchCategoryAdapter
import com.app.storego.databinding.ActCategorySearchBinding

class ActCategorySearch : AppCompatActivity() {
    private lateinit var binding:ActCategorySearchBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActCategorySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupAdapter()
        binding.ivBack.setOnClickListener {
            finish()
        }
    }


    private fun setupAdapter()
    {
        binding.rvCategories.apply {
            layoutManager=LinearLayoutManager(this@ActCategorySearch)
            adapter=SearchCategoryAdapter()
            isNestedScrollingEnabled=true
        }
    }
}