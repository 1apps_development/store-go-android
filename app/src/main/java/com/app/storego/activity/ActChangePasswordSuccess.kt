package com.app.storego.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.storego.MainActivity
import com.app.storego.databinding.ActChangePasswordSuccessBinding


class ActChangePasswordSuccess : AppCompatActivity() {
    private lateinit var binding: ActChangePasswordSuccessBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActChangePasswordSuccessBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tvHome.setOnClickListener {
            startActivity(Intent(this@ActChangePasswordSuccess, MainActivity::class.java))
        }
    }
}