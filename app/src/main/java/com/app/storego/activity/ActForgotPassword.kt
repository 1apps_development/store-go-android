package com.app.storego.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import com.app.storego.databinding.ActForgotPasswordBinding


class ActForgotPassword : AppCompatActivity() {

    private lateinit var binding: ActForgotPasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding= ActForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initClickListeners()
    }



     private fun initClickListeners() {

        binding.btnSendCode.setOnClickListener {
            startActivity(Intent(this@ActForgotPassword,ActOtp::class.java))
        }


    }

}