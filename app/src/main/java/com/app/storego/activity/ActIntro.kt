package com.app.storego.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.storego.databinding.ActIntroBinding

class ActIntro : AppCompatActivity() {
    private lateinit var binding: ActIntroBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActIntroBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.constraintLogin.setOnClickListener {

            startActivity(Intent(this@ActIntro, ActLogin::class.java))
        }

        binding.constraintRegister.setOnClickListener {
            startActivity(Intent(this@ActIntro, ActRegisterOption::class.java))

        }
    }
}