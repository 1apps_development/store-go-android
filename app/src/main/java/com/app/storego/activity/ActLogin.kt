package com.app.storego.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.storego.databinding.ActLoginBinding


class ActLogin : AppCompatActivity() {

    private lateinit var binding: ActLoginBinding



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.linearEmail.setOnClickListener {
            startActivity(Intent(this@ActLogin,ActEmailLogin::class.java))
        }
    }
    }