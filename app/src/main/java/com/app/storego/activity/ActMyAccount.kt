package com.app.storego.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.storego.R
import com.app.storego.databinding.ActMyAccountBinding

class ActMyAccount : AppCompatActivity() {

private lateinit var binding:ActMyAccountBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActMyAccountBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}