package com.app.storego.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.FrameLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.storego.R
import com.app.storego.adapter.RecentOrderAdapter
import com.app.storego.adapter.StaticsAdapter
import com.app.storego.databinding.ActOrderSearchBinding
import com.app.storego.databinding.DialogOrderDetailBinding
import com.app.storego.util.Common
import com.app.storego.util.Constants
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

class ActOrderSearch : AppCompatActivity() {
    private lateinit var binding:ActOrderSearchBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActOrderSearchBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setupAdapter()
        binding.ivBack.setOnClickListener {
            finish()
        }
    }

    private fun setupAdapter()
    {
        binding.rvOrders.apply {
            layoutManager= LinearLayoutManager(this@ActOrderSearch)
            adapter= RecentOrderAdapter { type: String, pos: Int ->
                run {
                    if(type==Constants.ItemClick)
                    {
                        orderDetailDialog()
                    }
                }
            }
            isNestedScrollingEnabled=true


        }
    }



    private fun orderDetailDialog() {
        val bottomSheetDialog = BottomSheetDialog(this@ActOrderSearch, R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet) ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) { Common.showFullScreenBottomSheet(bottomSheet) }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }

        val bottomSheetBinding = DialogOrderDetailBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)

        bottomSheetBinding.rvStatics.apply {
            val linearLayout=LinearLayoutManager(this@ActOrderSearch)
            adapter= StaticsAdapter()
            layoutManager=linearLayout
            addItemDecoration(DividerItemDecoration(context, linearLayout.orientation))
        }
        bottomSheetBinding.ivClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }
}