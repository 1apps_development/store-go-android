package com.app.storego.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import com.app.storego.databinding.ActOtpBinding


class ActOtp : AppCompatActivity() {
    private lateinit var binding: ActOtpBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActOtpBinding.inflate(layoutInflater)

        setContentView(binding.root)

        initClickListeners()
    }


     private fun initClickListeners() {


        binding.btnSendCode.setOnClickListener {
            startActivity(Intent(this@ActOtp, ActChangePasswordSuccess::class.java))
        }
    }
}