package com.app.storego.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.storego.adapter.ProductRatingAdapter
import com.app.storego.databinding.ActProductDetailBinding

class ActProductDetail : AppCompatActivity() {
    private lateinit var binding: ActProductDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActProductDetailBinding.inflate(layoutInflater)
        setupAdapter()
        setContentView(binding.root)
    }

    private fun setupAdapter()
    {
        binding.rvRattingList.apply {
            val linearLayout=LinearLayoutManager(this@ActProductDetail)
            layoutManager=linearLayout
            addItemDecoration(DividerItemDecoration(context, linearLayout.orientation))

            adapter=ProductRatingAdapter()
        }
        binding.ivBack.setOnClickListener {
            finish()

        }
    }
}