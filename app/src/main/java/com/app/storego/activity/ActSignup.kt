package com.app.storego.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.storego.MainActivity
import com.app.storego.databinding.ActSignupBinding


class ActSignup : AppCompatActivity() {
    private lateinit var binding:ActSignupBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActSignupBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnSignUp.setOnClickListener {
            startActivity(Intent(this@ActSignup, MainActivity::class.java))
        }
    }
}