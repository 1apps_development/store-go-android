package com.app.storego.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.app.storego.R


class ActSplash : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_splash)
        Handler(Looper.getMainLooper()).postDelayed({


            startActivity(Intent(this@ActSplash, ActIntro::class.java))
            finish()
        }, 2000)

    }

}