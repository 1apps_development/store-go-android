package com.app.storego.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.storego.databinding.ActStoreEmailSettingsBinding

class ActStoreEmailSettings : AppCompatActivity() {
    private lateinit var binding:ActStoreEmailSettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActStoreEmailSettingsBinding.inflate(layoutInflater)

        setContentView(binding.root)

        initClickListeners()
    }

    private fun initClickListeners()
    {
        binding.ivBack.setOnClickListener {
            finish()

        }
    }
}