package com.app.storego.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.storego.databinding.ActStorePaymentBinding

class ActStorePayment : AppCompatActivity() {
    private lateinit var binding:ActStorePaymentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding= ActStorePaymentBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initClickListeners()
    }


    private fun initClickListeners()
    {
        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}