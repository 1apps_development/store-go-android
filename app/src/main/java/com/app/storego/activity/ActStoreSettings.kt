package com.app.storego.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.storego.databinding.ActStoreSettingsBinding

class ActStoreSettings : AppCompatActivity() {

    private lateinit var binding:ActStoreSettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActStoreSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initClickListeners()
    }

    private fun initClickListeners()
    {
        binding.ivBack.setOnClickListener {
            finish()
        }
    }
}