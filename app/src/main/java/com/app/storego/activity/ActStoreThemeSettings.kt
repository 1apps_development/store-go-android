package com.app.storego.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.storego.R
import com.app.storego.adapter.ThemeAdapter
import com.app.storego.databinding.ActStoreThemeSettingsBinding

class ActStoreThemeSettings : AppCompatActivity() {

    private lateinit var binding:ActStoreThemeSettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding= ActStoreThemeSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initClickListeners()
        setupAdapter()
    }

    private fun initClickListeners()
    {
        binding.ivBack.setOnClickListener {
            finish()
        }
    }

    private fun setupAdapter()
    {
        binding.rvTemplateList.apply {
            layoutManager=GridLayoutManager(this@ActStoreThemeSettings,2,RecyclerView.VERTICAL,false)
            adapter=ThemeAdapter()
            isNestedScrollingEnabled=true
        }
    }
}