package com.app.storego.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.storego.R
import com.app.storego.databinding.ActWhatsappSettingsBinding

class ActWhatsAppSettings : AppCompatActivity() {
    private lateinit var binding:ActWhatsappSettingsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding= ActWhatsappSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initClickListeners()
    }

    private fun initClickListeners()
    {
        binding.ivBack.setOnClickListener {
            finish()
        }
    }

}