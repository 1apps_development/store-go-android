package com.app.storego.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.storego.databinding.CellGridProductBinding
import com.app.storego.util.Constants

class GridAdapter(var listener :(String,Int)->Unit) :RecyclerView.Adapter<GridAdapter.GridProductViewHolder>() {

inner class GridProductViewHolder(var itemBinding:CellGridProductBinding):RecyclerView.ViewHolder(itemBinding.root)
{
    fun bindItems(listener :(String,Int)->Unit,position: Int){
        itemBinding.apply {

        }
    }
}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridProductViewHolder {
      return  GridProductViewHolder(CellGridProductBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: GridProductViewHolder, position: Int) {
        holder.bindItems(listener,position)
    }

    override fun getItemCount(): Int {
      return  5
    }

}