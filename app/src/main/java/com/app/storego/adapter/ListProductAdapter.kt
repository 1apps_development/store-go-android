package com.app.storego.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.storego.databinding.CellGridProductBinding
import com.app.storego.databinding.CellProductBinding
import com.app.storego.util.Constants

class ListProductAdapter(var listener :(String,Int)->Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    val IS_LIST = 0
    val IS_GRID = 1
    private var mType = 0

    inner class ListProductViewHolder(private val itemBinding: CellProductBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bindItems(listener :(String,Int)->Unit,position: Int){
            itemBinding.apply {
                swipeLayout.surfaceView.setOnClickListener {
                    listener(Constants.ItemClick,position)

                }
            }
        }
    }

    inner class GridProductViewHolder(itemBinding: CellGridProductBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        Log.e("viewType",viewType.toString())
        return if(viewType==IS_LIST) {
            ListProductViewHolder(CellProductBinding.inflate(LayoutInflater.from(parent.context), parent, false))


        }else {
            GridProductViewHolder(CellGridProductBinding.inflate(LayoutInflater.from(parent.context), parent, false))

        }



    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {


        Log.e("posttion",getItemViewType(position).toString())

        val listProductViewHolder: ListProductViewHolder = holder as ListProductViewHolder
        listProductViewHolder.bindItems(listener,position)
    }


    fun setViewType(type: Int) {
        mType = type
    }

    fun getViewType():Int
    {
        return mType
    }

    override fun getItemCount(): Int {
        return 5
    }
}