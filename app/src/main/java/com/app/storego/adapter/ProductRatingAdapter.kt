package com.app.storego.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.storego.databinding.CellRattingBinding

class ProductRatingAdapter:RecyclerView.Adapter<ProductRatingAdapter.ProductViewHolder>() {

    inner class ProductViewHolder(itemBinding: CellRattingBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductRatingAdapter.ProductViewHolder {
        val view=CellRattingBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ProductViewHolder(view)

    }

    override fun onBindViewHolder(holder: ProductRatingAdapter.ProductViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}