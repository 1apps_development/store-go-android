package com.app.storego.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.storego.databinding.CellRecentOrdersBinding
import com.app.storego.util.Constants

class RecentOrderAdapter(var listener:(String,Int)->Unit) :RecyclerView.Adapter<RecentOrderAdapter.RecentOrderViewHolder>() {

    inner class RecentOrderViewHolder(var binding:CellRecentOrdersBinding):RecyclerView.ViewHolder(binding.root)
    {
         fun bindItems( listener:(String,Int)->Unit,position: Int)
        {
            binding.apply {
                swipeLayout.surfaceView.setOnClickListener {
                    listener(Constants.ItemClick,position)

                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentOrderViewHolder {
        val view=CellRecentOrdersBinding.inflate(LayoutInflater.from(parent.context),parent,false)

        return RecentOrderViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecentOrderViewHolder, position: Int) {


        holder.bindItems(listener,position)
     /*   holder.itemView.setOnClickListener {
            listener(Constants.ItemClick,position)
        }*/

    }

    override fun getItemCount(): Int {
            return 5
    }
}