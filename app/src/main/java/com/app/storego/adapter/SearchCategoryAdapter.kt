package com.app.storego.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.storego.databinding.CellSearchCategoryBinding

class SearchCategoryAdapter():RecyclerView.Adapter<SearchCategoryAdapter.SearchCategoryViewHolder>() {

    inner  class SearchCategoryViewHolder(itemBinding:CellSearchCategoryBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchCategoryViewHolder {
        val view=CellSearchCategoryBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return SearchCategoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: SearchCategoryViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}