package com.app.storego.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.storego.databinding.CellStaticsOrderDetailBinding

class StaticsAdapter():RecyclerView.Adapter<StaticsAdapter.StaticsClassViewHolder>() {

    inner class StaticsClassViewHolder(var binding :CellStaticsOrderDetailBinding): RecyclerView.ViewHolder(binding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StaticsClassViewHolder {
        val view=CellStaticsOrderDetailBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return StaticsClassViewHolder(view)
    }

    override fun onBindViewHolder(holder: StaticsClassViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {
        return 5
    }
}