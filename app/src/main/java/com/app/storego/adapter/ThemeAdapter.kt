package com.app.storego.adapter

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.storego.databinding.CellTemplateBinding

class ThemeAdapter :RecyclerView.Adapter<ThemeAdapter.ThemeViewHolder>() {

    inner class ThemeViewHolder(itemBinding:CellTemplateBinding):RecyclerView.ViewHolder(itemBinding.root)
    {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThemeViewHolder {

        return ThemeViewHolder(CellTemplateBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun onBindViewHolder(holder: ThemeViewHolder, position: Int) {
    }

    override fun getItemCount(): Int {

        return 6
    }
}