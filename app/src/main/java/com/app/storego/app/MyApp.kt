package com.app.storego.app

import android.app.Application
import androidx.multidex.MultiDex
import com.app.storego.R
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump

class MyApp :Application() {

    companion object {
        lateinit var app: MyApp



        fun getInstance(): MyApp {
            return app
        }
    }
    override fun onCreate() {
        super.onCreate()

        app = this
     /*      ViewPump.init(
               ViewPump.builder()
                   .addInterceptor(
                       CalligraphyInterceptor(
                           CalligraphyConfig.Builder()
                               .setDefaultFontPath("font/poppins_regular.ttf")
                               .setFontAttrId(R.attr.fontPath)
                               .build()
                       )
                   )
                   .build()
           )*/
        MultiDex.install(this)

    }
}