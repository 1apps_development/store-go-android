package com.app.storego.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.storego.R
import com.app.storego.activity.ActCategorySearch
import com.app.storego.adapter.CategoriesAdapter
import com.app.storego.databinding.DialogAddNewCategoryBinding
import com.app.storego.databinding.FragCategoriesBinding
import com.google.android.material.bottomsheet.BottomSheetDialog


class FragCategories : Fragment() {
    private lateinit var binding:FragCategoriesBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragCategoriesBinding.inflate(layoutInflater)
        setAdapter()
        return binding.root
    }


    private fun setAdapter()
    {
        val linearLayoutManager=LinearLayoutManager(requireActivity())

        binding.rvCategoryList.apply {
            layoutManager=linearLayoutManager
            addItemDecoration(  DividerItemDecoration(context, linearLayoutManager.orientation))
            adapter=CategoriesAdapter()
        }

        binding.linearSearch.setOnClickListener {

            startActivity(Intent(requireActivity(),ActCategorySearch::class.java))
        }
        binding.constraintQuickAdd.setOnClickListener {
            newCategoryDialog()
        }

    }


    private fun newCategoryDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)


        val bottomSheetBinding = DialogAddNewCategoryBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)
        bottomSheetBinding.ivClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }


}