package com.app.storego.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.storego.R
import com.app.storego.adapter.ProductAdapter
import com.app.storego.adapter.RecentOrderAdapter
import com.app.storego.adapter.StaticsAdapter
import com.app.storego.databinding.DialogOrderDetailBinding
import com.app.storego.databinding.FragDashBoardBinding
import com.app.storego.util.Common
import com.app.storego.util.Constants
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.linroid.filtermenu.library.FilterMenu
import com.linroid.filtermenu.library.FilterMenuLayout


class FragDashBoard : Fragment() {

    private lateinit var binding: FragDashBoardBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragDashBoardBinding.inflate(layoutInflater)
        setupAdapter()
        attachMenu(binding.filterMenu)

        return binding.root
    }

    private fun setupAdapter() {
        binding.rvTopProducts.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = ProductAdapter()
            isNestedScrollingEnabled = true


        }

        binding.rvRecentOrders.apply {
            layoutManager = LinearLayoutManager(requireActivity())
            adapter = RecentOrderAdapter { type: String, pos: Int ->
                run {
                    if(type == Constants.ItemClick) {
                        orderDetailDialog()
                    }
                }

            }
            isNestedScrollingEnabled = true


        }
    }


    private fun orderDetailDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet) ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) { Common.showFullScreenBottomSheet(bottomSheet) }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }

        val bottomSheetBinding = DialogOrderDetailBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)

        bottomSheetBinding.rvStatics.apply {
            val linearLayout=LinearLayoutManager(requireActivity())
            adapter= StaticsAdapter()
            layoutManager=linearLayout
            addItemDecoration(DividerItemDecoration(context, linearLayout.orientation))
        }
        bottomSheetBinding.ivClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }


    private val listener: FilterMenu.OnMenuChangeListener = object :
        FilterMenu.OnMenuChangeListener {
        override fun onMenuItemClick(view: View, position: Int) {
            Toast.makeText(requireActivity(), "Touched position $position", Toast.LENGTH_SHORT)
                .show()


        }


        override fun onMenuCollapse() {

        }

        override fun onMenuExpand() {

        }
    }

    private fun attachMenu(layout: FilterMenuLayout): FilterMenu? {
        return FilterMenu.Builder(requireActivity())
            .addItem(R.drawable.ic_menu_category)
            .addItem(R.drawable.ic_menu_home)
            .addItem(R.drawable.ic_menu_products)
            .attach(layout)
            .withListener(listener)
            .build()
    }


}