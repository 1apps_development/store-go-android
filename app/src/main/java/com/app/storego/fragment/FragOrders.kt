package com.app.storego.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.storego.R
import com.app.storego.activity.ActOrderSearch
import com.app.storego.adapter.RecentOrderAdapter
import com.app.storego.adapter.StaticsAdapter
import com.app.storego.databinding.DialogAddNewCategoryBinding
import com.app.storego.databinding.DialogAddNewOrderBinding
import com.app.storego.databinding.DialogOrderDetailBinding
import com.app.storego.databinding.FragOrdersBinding
import com.app.storego.util.Common
import com.app.storego.util.Constants
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

class FragOrders : Fragment() {
private lateinit var binding:FragOrdersBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
            binding= FragOrdersBinding.inflate(layoutInflater)
            setupAdapter()
        initClickListeners()
        return binding.root
    }


    private fun setupAdapter()
    {
        binding.rvOrders.apply {
            layoutManager= LinearLayoutManager(requireActivity())
            adapter= RecentOrderAdapter { type: String, pos: Int ->
                    if(type==Constants.ItemClick)
                    {
                        orderDetailDialog()
                    }
            }
            isNestedScrollingEnabled=true


        }
    }

    private fun initClickListeners()
    {
        binding.linearSearch.setOnClickListener {
            startActivity(Intent(requireActivity(),ActOrderSearch::class.java))
        }

        binding.constraintAddOrder.setOnClickListener {
            newOrderDialog()
        }
    }

    private fun orderDetailDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet) ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) { Common.showFullScreenBottomSheet(bottomSheet) }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }

        val bottomSheetBinding = DialogOrderDetailBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)

        bottomSheetBinding.rvStatics.apply {
            val linearLayout=LinearLayoutManager(requireActivity())
            adapter=StaticsAdapter()
            layoutManager=linearLayout
            addItemDecoration(DividerItemDecoration(context, linearLayout.orientation))
        }
        bottomSheetBinding.ivClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }


    private fun newOrderDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet) ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) { Common.showFullScreenBottomSheet(bottomSheet) }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }
        val bottomSheetBinding = DialogAddNewOrderBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)

        bottomSheetBinding.ivClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }


}