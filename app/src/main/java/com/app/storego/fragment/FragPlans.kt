package com.app.storego.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.storego.R
import com.app.storego.databinding.FragPlansBinding


class FragPlans : Fragment() {

private lateinit var binding:FragPlansBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragPlansBinding.inflate(layoutInflater)
        return binding.root
    }


}