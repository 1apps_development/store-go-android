package com.app.storego.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.app.ActivityCompat.invalidateOptionsMenu
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.storego.R
import com.app.storego.activity.ActProductDetail
import com.app.storego.adapter.GridAdapter
import com.app.storego.adapter.ListProductAdapter
import com.app.storego.databinding.DialogAddNewOrderBinding
import com.app.storego.databinding.DialogNewProductBinding
import com.app.storego.databinding.FragProductsBinding
import com.app.storego.util.Common
import com.app.storego.util.Constants
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog


class FragProducts : Fragment() {
    private lateinit var binding: FragProductsBinding
    private lateinit var productAdapter: ListProductAdapter
    private var linearLayoutManager:LinearLayoutManager?=null
    private var gridLayoutManager:GridLayoutManager?=null
    private var isList=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        linearLayoutManager= LinearLayoutManager(requireActivity())
        gridLayoutManager= GridLayoutManager(requireActivity(),2,RecyclerView.VERTICAL,false)
        binding = FragProductsBinding.inflate(layoutInflater)
        setupAdapter()
        initClickListeners()


        return binding.root


    }

    private fun setupAdapter() {
        productAdapter = ListProductAdapter { type: String, pos: Int ->
            if (type == Constants.ItemClick) {
                startActivity(Intent(requireActivity(), ActProductDetail::class.java))
            }
        }

        binding.rvProducts.apply {
            binding.rvProducts.layoutManager =linearLayoutManager
            adapter = productAdapter
            isNestedScrollingEnabled = true
        }
    }


    private fun initClickListeners()
    {

        binding.constraintAddProduct.setOnClickListener {
            newProductDialog()
        }
        binding.tvLayoutType.setOnClickListener {
            binding.rvProducts.invalidateItemDecorations()

            isList = if(isList==0) {
                binding.tvLayoutType.text= resources.getString(R.string.gridview)

                gridAdapter()
                1


            }else {
                binding.tvLayoutType.text= resources.getString(R.string.list_view)

                listAdapter()
                0
            }


          /*  if(productAdapter.getViewType()==0)
            {
                productAdapter.setViewType(1)
            }else
            {
                productAdapter.setViewType(0)

            }
            productAdapter.notifyDataSetChanged()

            binding.rvProducts.invalidateItemDecorations()

            binding.rvProducts.layoutManager = if (productAdapter.getViewType()== 0) linearLayoutManager else gridLayoutManager
                binding.tvLayoutType.text=if (productAdapter.getViewType()== 0) resources.getString(R.string.list_view) else resources.getString(R.string.gridview)
                productAdapter.notifyDataSetChanged()*/

        }
    }

    private fun newProductDialog() {
        val bottomSheetDialog = BottomSheetDialog(requireActivity(), R.style.BottomSheetDialogTheme)
        bottomSheetDialog.setOnShowListener {
            val bottomSheet: FrameLayout = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet) ?: return@setOnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            if (bottomSheet.layoutParams != null) { Common.showFullScreenBottomSheet(bottomSheet) }

            bottomSheet.setBackgroundResource(android.R.color.transparent)
            Common.expandBottomSheet(bottomSheetBehavior)
        }
        val bottomSheetBinding = DialogNewProductBinding.inflate(layoutInflater)
        bottomSheetDialog.setContentView(bottomSheetBinding.root)

        bottomSheetBinding.ivClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()

    }



    private fun gridAdapter()
    {
        binding.rvProducts.apply {
            binding.rvProducts.layoutManager = gridLayoutManager
            adapter = GridAdapter{ type: String, pos: Int ->
                if (type == Constants.ItemClick) {
                    startActivity(Intent(requireActivity(), ActProductDetail::class.java))
                }
            }
            isNestedScrollingEnabled = true
        }
    }

    private fun listAdapter()
    {
        binding.rvProducts.apply {
            binding.rvProducts.layoutManager = linearLayoutManager
            adapter = productAdapter
            isNestedScrollingEnabled = true
        }
    }


}