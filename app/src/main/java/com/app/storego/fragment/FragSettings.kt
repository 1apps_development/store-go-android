package com.app.storego.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.storego.R
import com.app.storego.activity.*
import com.app.storego.databinding.FragSettingsBinding


class FragSettings : Fragment() {
    private lateinit var binding:FragSettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragSettingsBinding.inflate(layoutInflater)
        initClickListeners()
        return binding.root
    }

    private fun initClickListeners()
    {
        binding.constraintMyAccount.setOnClickListener {
            startActivity(Intent(requireActivity(),ActMyAccount::class.java))
        }
        binding.constraintStoreSettings.setOnClickListener {
            startActivity(Intent(requireActivity(),ActStoreSettings::class.java))

        }
        binding .constraintStoreEmail.setOnClickListener {

            startActivity(Intent(requireActivity(),ActStoreEmailSettings::class.java))

        }


        binding.constraintStorePayment.setOnClickListener {
            startActivity(Intent(requireActivity(),ActStorePayment::class.java))

        }

        binding.constraintThemeSettings.setOnClickListener {
            startActivity(Intent(requireActivity(),ActStoreThemeSettings::class.java))

        }

        binding.constraintWhatsAppSettings.setOnClickListener {
            startActivity(Intent(requireActivity(),ActWhatsAppSettings::class.java))

        }
    }


}