package com.app.storego.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.storego.R
import com.app.storego.adapter.TopUrlAdapter
import com.app.storego.databinding.FragStatisticsBinding


class FragStatistics : Fragment() {

        private lateinit var binding:FragStatisticsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding= FragStatisticsBinding.inflate(layoutInflater)
        setupAdapter()
        return binding.root
    }
    private fun setupAdapter()
    {
        binding.rvUrlList.apply {
            layoutManager=LinearLayoutManager(requireActivity())
            adapter=TopUrlAdapter()
            isNestedScrollingEnabled=true
        }
    }


}